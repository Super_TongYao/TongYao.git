<div align=center><br/>
   <!--<img width="150" height="150" src="https://images.gitee.com/uploads/images/2021/0514/171832_e89a8402_2100316.png"/><br/><br/> -->
    基于轻量级SpringBoot和shiro安全框架，打造傻瓜式业务编写系统,<br/><br/>
    即下即用，内置代码生成器直接开始写需求业务逻辑，不需要重新搭建系统框架。<br/><br/>
    <a href="http://spring.io/projects/spring-boot">
    	<img src="https://img.shields.io/badge/spring--boot-2.2.6-green.svg" alt="spring-boot">
    </a>
    <a href="http://mp.baomidou.com">
    	<img src="https://img.shields.io/badge/mybatis--plus-3.3.1-blue.svg" alt="mybatis-plus">
    </a> 
    <a href="http://mp.shiro.com">
        <img src="https://img.shields.io/badge/shiro-1.4.6-green.svg" alt="shiro">
    </a> <br/>
</div>

### 框架描述
TongYao 基于SpringBoot2.x + Shiro + MybatisPlus打造的纯后台权限框架。很好的结合了shiro安全框架角色菜单打造的轻量级微型后台系统，可用作于微信小程序作为后台程序。

该框架配备了Shiro安全集成、用户管理、菜单管理、角色权限管理，还配备了核心业务代码生成器，从而省去大量的环境框架搭建成本，节省了开发工作者的时间，以提高效率。

 **该框架为前后台分离框架，需配合前端TongYao_Html-admin框架也可以配合你的自定义前端框架操作执行！** 

我们又基于Springboot2.x，jwt，security整合了TongYao2.0地址：[https://gitee.com/Super_TongYao/TongYao2.0](https://gitee.com/Super_TongYao/TongYao2.0)
### 注意事项

本分支为TongYao最新分支，不要下master分支！ 不要下master分支！ 不要下master分支！

如需HTML版本分支请点击master分支，切换HTML版本！

### 实战演示
TongYao框架实战演示地址：<a href="http://82.157.190.245/" target="_blank">传送门</a>


 **使用TongYao搭配的前端框架或使用你的自定义前端框架** 
| 序号 | 名称           | 地址 |
|----|--------------|----|
| 1  | TongYao      |https://gitee.com/Super_TongYao/TongYao.git    |
| 2  | TongYao_Html-admin |https://gitee.com/Super_TongYao/tong-yao_-html-admin.git    |







本代码还有很多欠缺，作者时间有限，抽时间一点一点更新。有兴趣的小伙伴，V：zty520r 欢迎一起沟通贡献你的代码。