package com.example.tongyao.utils.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * manager-for-isp >>> 【cn.com.testor.util】
 * 日期时间工具类
 *
 * @author: tongyao
 * @since: 2022-06-23 10:31
 */
public class TyDateUtils {

    public final static String YYYY = "yyyy";
    public final static String MM = "MM";
    public final static String DD = "DD";
    public final static String HH = "HH";

    public final static String YYYY_MM = "yyyy-MM";
    public final static String YYYY_MM_DD = "yyyy-MM-dd";
    public final static String YYYY_MM_DD_HH = "yyyy-MM-dd HH";
    public final static String YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";
    public final static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";



    public final static String YYYY__MM = "yyyy/MM";
    public final static String YYYY__MM__DD = "yyyy/MM/dd";
    public final static String YYYY__MM__DD_HH = "yyyy/MM/dd HH";
    public final static String YYYY__MM__DD_HH_MM = "yyyy/MM/dd HH:mm";
    public final static String YYYY__MM__DD_HH_MM_SS = "yyyy/MM/dd HH:mm:ss";


    public final static String YYYY___MM = "yyyy.MM";
    public final static String YYYY___MM___DD = "yyyy.MM.dd";
    public final static String YYYY___MM___DD_HH = "yyyy/MM/dd HH";
    public final static String YYYY___MM___DD_HH_MM = "yyyy.MM.dd HH:mm";
    public final static String YYYY___MM___DD_HH_MM_SS = "yyyy.MM.dd HH:mm:ss";


    public final static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    public final static String YYYY_MM_DD_T_HH_MM_SS = "yyyy-MM-ddTHH:mm:ss";

    static SimpleDateFormat simpleDateFormat = null;
    static Calendar calendar1 = null;
    static Calendar calendar2 = null;
    static Date date = null;
    static Object obj = null;
    static String[] dateArray = null;
    static Object[] objArray = null;


    static Map<String,String> maps = new HashMap<>();

    /**
     * 获得指定日期的开始时分秒
     * @param dateStr
     * @return
     */
    public static String getCustomDateStartTime(String dateStr){
        return dateStr.trim()+" 00:00:00";
    }
    public static String getCustomDateStartTimeT(String dateStr){
        return dateStr.trim()+"T00:00:00";
    }

    /**
     * 获得指定日期的结束时分秒
     * @param dateStr
     * @return
     */
    public static String getCustomDateEndTime(String dateStr){
        return dateStr.trim()+" 23:59:59";
    }
    public static String getCustomDateEndTimeT(String dateStr){
        return dateStr.trim()+"T23:59:59";
    }

    /**
     * 获得指定日期的开始结束时分秒
     * @param dateStr
     * @return
     */
    public static String[] getCustomDateTime(String dateStr){
        dateArray = new String[2];
        dateArray[0] = dateStr.trim()+" 00:00:00";
        dateArray[0] = dateStr.trim()+" 23:59:59";
        return dateArray;
    }
    public static String[] getCustomDateTimeT(String dateStr){
        dateArray = new String[2];
        dateArray[0] = dateStr.trim()+"T00:00:00";
        dateArray[0] = dateStr.trim()+"T00:00:00";
        return dateArray;
    }

    /**
     * 获得指定日期的昨天日期
     * @param dateStr 日期参数
     * @param returnType 返回类型
     * @return String/Date类型等
     */
    public static Object getCustomYesterday(String dateStr, String returnType){
        try {
            simpleDateFormat = new SimpleDateFormat(YYYY_MM_DD);
            calendar1 = new GregorianCalendar();
            calendar1.setTime(simpleDateFormat.parse(dateStr));
            calendar1.add(Calendar.DAY_OF_MONTH, -1);

            date = calendar1.getTime();
            switch (returnType){
                case "String":
                    obj = simpleDateFormat.format(date);
                    break;
                case "Date":
                    obj = date;
                    break;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return obj;
    }

    /**
     * 获得指定日期的自然周开始日期
     * @param dateStr
     * @param returnType
     * @return
     */
    public static Object getCustomWeekStartDate(String dateStr, String returnType) {
        try {
            simpleDateFormat = new SimpleDateFormat(YYYY_MM_DD);
            date = simpleDateFormat.parse(dateStr);
            calendar1 = Calendar.getInstance();
            calendar1.setTime(date);

            int dayofweek = calendar1.get(Calendar.DAY_OF_WEEK);
            if(dayofweek == 1){
                dayofweek += 7;
            }

            calendar1.add(Calendar.DATE, 2 - dayofweek);

            calendar2 = Calendar.getInstance();
            calendar2.setTime(calendar1.getTime());

            date = calendar2.getTime();
            switch (returnType){
                case "String":
                    obj = simpleDateFormat.format(date);
                    break;
                case "Date":
                    obj = date;
                    break;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return obj;
    }


    /**
     * 获得指定日期的自然周结束日期
     * @param dateStr
     * @param returnType
     * @return
     */
    public static Object getCustomWeekEndDate(String dateStr, String returnType) {
        try {
            simpleDateFormat = new SimpleDateFormat(YYYY_MM_DD);
            date = simpleDateFormat.parse(dateStr);
            calendar1 = Calendar.getInstance();
            calendar1.setTime(date);

            int dayofweek = calendar1.get(Calendar.DAY_OF_WEEK);
            if(dayofweek == 1){
                dayofweek += 7;
            }

            calendar1.add(Calendar.DATE, 2 - dayofweek);

            calendar2 = Calendar.getInstance();
            calendar2.setTime(calendar1.getTime());

            calendar1.setTime(calendar2.getTime());
            calendar1.add(Calendar.DAY_OF_WEEK, 6);

            calendar2 = Calendar.getInstance();
            calendar2.setTime(calendar1.getTime());

            date = calendar2.getTime();
            switch (returnType){
                case "String":
                    obj = simpleDateFormat.format(date);
                    break;
                case "Date":
                    obj = date;
                    break;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return obj;
    }

    /**
     * 获得指定日期的自然周开始结束日期
     * @param dateStr
     * @param returnType
     * @return
     */
    public static Object[] getCustomWeekDate(String dateStr, String returnType) {
        try {
            objArray = new Object[2];
            simpleDateFormat = new SimpleDateFormat(YYYY_MM_DD);
            date = simpleDateFormat.parse(dateStr);
            calendar1 = Calendar.getInstance();
            calendar1.setTime(date);

            int dayofweek = calendar1.get(Calendar.DAY_OF_WEEK);
            if(dayofweek == 1){
                dayofweek += 7;
            }

            calendar1.add(Calendar.DATE, 2 - dayofweek);

            calendar2 = Calendar.getInstance();
            calendar2.setTime(calendar1.getTime());

            // 开始时间
            objArray[0] = calendar2.getTime();

            calendar1.setTime(calendar2.getTime());
            calendar1.add(Calendar.DAY_OF_WEEK, 6);

            calendar2 = Calendar.getInstance();
            calendar2.setTime(calendar1.getTime());

            // 结束时间
            objArray[1] = calendar2.getTime();
            switch (returnType){
                case "String":
                    objArray[0] = simpleDateFormat.format(objArray[0]);
                    objArray[1] = simpleDateFormat.format(objArray[1]);
                    break;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return objArray;
    }


    /**
     * 获得指定日期的自然月开始结束日期
     * @param dateStr
     * @param returnType
     * @return
     */
    public static Object getCustomMonthStartDate(String dateStr, String returnType) {
        try {
            simpleDateFormat = new SimpleDateFormat(YYYY_MM);
            date = simpleDateFormat.parse(dateStr);

            String result = simpleDateFormat.format(date)+"-01";
            switch (returnType){
                case "String":
                    obj = result;
                    break;
                case "Date":
                    obj = simpleDateFormat.parse(result);
                    break;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return obj;
    }

    /**
     * 获取本月的开始结束时间
     * @param dateStr
     * @param returnType
     * @return
     */
    public static Object getCustomMonthEndDate(String dateStr, String returnType) {
        try {
            simpleDateFormat = new SimpleDateFormat(YYYY_MM);
            date = simpleDateFormat.parse(dateStr);

            calendar1 = Calendar.getInstance();
            calendar1.setTime(date);

            String result = simpleDateFormat.format(date)+"-"+calendar1.getActualMaximum(5);
            switch (returnType){
                case "String":
                    obj = result;
                    break;
                case "Date":
                    obj = simpleDateFormat.parse(result);
                    break;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static Object[] getCustomMonthDate(String dateStr, String returnType) {
        objArray = new Object[2];
        objArray[0] = getCustomMonthStartDate(dateStr,returnType);
        objArray[1] = getCustomMonthEndDate(dateStr,returnType);
        return objArray;
    }

    /*public static Object formateDateToHH*/


    public static void main(String[] args) {
        Object[] objects = getCustomMonthDate("2022-06-23","String");
        System.out.println(objects[0]+"   "+objects[1]);

    }

}
