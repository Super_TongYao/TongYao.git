package com.example.tongyao.utils.tools;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 公用类
 *
 * @version 1.0
 * @author tongyao
 * @since 2020-06-13
 */
public class PubClass {

    /**
     * 获得数字随机数
     *
     * @param numberCount
     * @return
     */
    public static String getRandomNumber(int numberCount){
        String numberStr = "";
        Random ra = new Random();
        for (int i = 0;i < numberCount;i++){
            numberStr += ra.nextInt(10)+"";
        }
        return  numberStr;
    }

    /**
     * 获得字母随机数
     *
     * @param numberCount
     * @return
     */
    public static String getLetterRandomNumber(int numberCount){
        String[] number = new String[]{"0","1","2","3","4","5","6","7","8","9",
                "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
                "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
        String numberStr = "";
        Random ra = new Random();
        for (int i = 0;i < numberCount;i++){
            numberStr += number[ra.nextInt(62)];
        }
        return  numberStr;
    }

    /**
     * 获得限制数字大小的随机数
     *
     * @param numberLen
     * @return
     */
    public static int getRandomNumberSize(int numberLen){
        Random ra = new Random();
        return  ra.nextInt(numberLen);
    }

    /**
     * 获得当前系统的年月日
     *
     * @return
     */
    public static String getDate(){
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date date = new Date();
        String dates = simpleDateFormat.format(date);
        return dates;
    }

    /**
     * 获得当前系统的当前秒数
     *
     * @return
     */
    public static String getTime(){
        String pattern = "HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date date = new Date();
        String time = simpleDateFormat.format(date);
        return time;
    }

    /**
     * 获得当前系统的年月日秒数
     *
     * @return
     */
    public static Date getDateTime(){
        String pattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date date = new Date();
        try {
            date = simpleDateFormat.parse(simpleDateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    /**
     * 获取今年是哪一年
     *
     * @return
     */
    public static int getNowYear() {
        Date date = new Date();
        GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
        gc.setTime(date);
        return Integer.valueOf(gc.get(1));
    }

    /**
     * 获取本月是哪一月
     *
     * @return
     */
    public static int getNowMonth() {
        Date date = new Date();
        GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
        gc.setTime(date);
        return gc.get(2) + 1;
    }

    /**
     * 根据当前日期获取前x天日期
     *
     * @param dayCount 前几天
     * @return
     */
    public static String getFirstxDayCount(int dayCount){
        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );
        Date today = new Date();
        Calendar theCa = Calendar.getInstance();
        theCa.setTime(today);
        theCa.add(theCa.DATE, + dayCount); //最后一个数字30可改，30天的意思
        Date start = theCa.getTime();
        return sdf.format(start);
    }

    /**
     * 获取昨天的开始时间
     *
     * @return
     */
    public static String getYesterdayDate() {
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Calendar cal2 = new GregorianCalendar();
        cal2.setTime(cal.getTime());
        cal2.add(Calendar.DAY_OF_MONTH, -1);
        return new Timestamp(cal2.getTimeInMillis()).toString().split(" ")[0];
    }

    /**
     * 获取明天的开始时间
     *
     * @return
     */
    public static String getTomorrowDate() {
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Calendar cal2 = new GregorianCalendar();
        cal2.setTime(cal.getTime());
        cal2.add(Calendar.DAY_OF_MONTH, 1);
        return new Timestamp(cal2.getTimeInMillis()).toString().split(" ")[0];
    }

    /**
     * 根据开始结束日期获得中间所有天数数量
     *
     * @param starTime 开始日期
     * @param endTime   结束日期
     * @return
     */
    public static String byStartEndDateGetMiddleDay(String starTime,String endTime){
        SimpleDateFormat formatter =   new SimpleDateFormat( "yyyy-MM-dd");
        Date date1= null;
        Date date = null;
        Long l = 0L;
        try {
            date = formatter.parse(endTime);
            long ts = date.getTime();
            date1 =  formatter.parse(starTime);
            long ts1 = date1.getTime();

            l = (ts - ts1) / (1000 * 60 * 60 * 24);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return l.intValue() + "";

    }

    /**
     * 获得本周开始结束日期
     *
     * @return
     */
    public static String[] getWeekStartEndDate(){
        Calendar ca=Calendar.getInstance();
        SimpleDateFormat f=new SimpleDateFormat("yyyy-MM-dd");
        int dayOfWeek = ca.get(Calendar.DAY_OF_WEEK);
        //中国习惯：周一是一周的开始
        if (dayOfWeek==1) {
            dayOfWeek=7;

        }else{
            dayOfWeek--;

        }
        Calendar  cal=(Calendar)ca.clone();
        cal.add(Calendar.DATE,1-dayOfWeek);
        Date date1=cal.getTime();
        cal=(Calendar)ca.clone();
        cal.add(Calendar.DATE,7-dayOfWeek);
        Date date2=cal.getTime();
        String str1=f.format(date1);
        String str2=f.format(date2);
        String[] array = new String[2];
        array[0] = str1;
        array[1] = str2;
        return array;
    }

    /**
     * 获取本月的开始结束日期
     *
     * @return
     */
    public static String[] getOriginaMonthStartEndDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(getNowYear(), getNowMonth() - 1, 1);


        Calendar calendar3 = Calendar.getInstance();
        if(null != calendar.getTime()) calendar3.setTime(calendar.getTime());
        calendar3.set(calendar3.get(Calendar.YEAR), calendar3.get(Calendar.MONTH),calendar3.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        calendar3.set(Calendar.MILLISECOND, 0);

        String[] array = new String[2];
        array[0] = new Timestamp(calendar3.getTimeInMillis()).toString().toString().split(" ")[0];

        Calendar calendar2 = Calendar.getInstance();
        calendar2.set(getNowYear(), getNowMonth() - 1, 1);
        int day = calendar2.getActualMaximum(5);
        calendar2.set(getNowYear(), getNowMonth() - 1, day);

        Calendar calendar4 = Calendar.getInstance();
        if(null != calendar2.getTime()) calendar4.setTime(calendar2.getTime());
        calendar4.set(calendar4.get(Calendar.YEAR), calendar4.get(Calendar.MONTH),    calendar4.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
        calendar4.set(Calendar.MILLISECOND, 999);
        array[1] = new Timestamp(calendar4.getTimeInMillis()).toString().split(" ")[0];
        return array;
    }


    /**
     * 获取某月的开始结束日期
     *
     * @param date
     * @return
     */
    public static String[] getMonthStartEndDate(String date) {
        String[] dateStr = date.split("-");
        int year = Integer.parseInt(dateStr[0]);
        int month = Integer.parseInt(dateStr[1]);

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, 1);

        String[] array = new String[2];
        array[0] = new Timestamp(calendar.getTimeInMillis()).toString();


        Calendar calendar2 = Calendar.getInstance();
        calendar2.set(year, month - 1, 1);
        int day = calendar2.getActualMaximum(5);
        calendar2.set(year, month - 1, day);
        array[1] = new Timestamp(calendar2.getTimeInMillis()).toString();
        return array;
    }

    /**
     * 获取本年的开始时间
     *
     * @return
     */
    public static String[] getYearStartEndDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, getNowYear());
        // cal.set
        cal.set(Calendar.MONTH, Calendar.JANUARY);
        cal.set(Calendar.DATE, 1);

        Calendar cal2 = Calendar.getInstance();
        cal2.set(Calendar.YEAR, getNowYear());
        cal2.set(Calendar.MONTH, Calendar.DECEMBER);
        cal2.set(Calendar.DATE, 31);

        Calendar calendar3 = Calendar.getInstance();
        if(null != cal.getTime()) calendar3.setTime(cal.getTime());
        calendar3.set(calendar3.get(Calendar.YEAR), calendar3.get(Calendar.MONTH),calendar3.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        calendar3.set(Calendar.MILLISECOND, 0);

        String[] array = new String[2];
        array[0] = new Timestamp(calendar3.getTimeInMillis()).toString().split(" ")[0];


        Calendar calendar4 = Calendar.getInstance();
        if(null != cal2.getTime()) calendar4.setTime(cal2.getTime());
        calendar4.set(calendar4.get(Calendar.YEAR), calendar4.get(Calendar.MONTH),    calendar4.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
        calendar4.set(Calendar.MILLISECOND, 999);
        array[1] = new Timestamp(calendar4.getTimeInMillis()).toString().split(" ")[0];
        return array;
    }

    /**
     * 获取开始与结束时间之间的日期列表
     * @param startDate
     * @param endDate
     * @return
     */
    public static List<String> getBetweenDateList(String startDate, String endDate) throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startTime = simpleDateFormat.parse(startDate);
        Date endTime = simpleDateFormat.parse(endDate);
        List<String> dateList = new ArrayList<String>();
        while(startTime.compareTo(endTime) != 1) {
            dateList.add(simpleDateFormat.format(startTime));
            startTime.setTime(startTime.getTime() + 1000 * 60 * 60 * 24);
        }
        return dateList;
    }

    //根据日期取得星期几
    public static String getWeek(String time) throws Exception{
        Calendar ca = Calendar.getInstance();
        String[] str = time.split("-");
        ca.set(Integer.parseInt(str[0]),Integer.parseInt(str[1])-1,Integer.parseInt(str[2]));
        String[] weekDays = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
        int index = ca.get(Calendar.DAY_OF_WEEK)-1;
        return weekDays[index];
    }

    /**
     * 获得项目路径
     * 
     * @return
     */
    public String getPath(){
        return System.getProperty("user.dir");
    }

    /**
     * 获取项目根路径
     * @return
     */
    public String getProjectPath(){
        String tomcatWebapp = "webapps";

        String path = new File(this.getClass().getResource("/").getPath()).toString();
        if(path.indexOf(tomcatWebapp) != -1){
            String webappsPath = path.substring(path.indexOf(tomcatWebapp),path.length());
            String projectPath = webappsPath.substring(webappsPath.indexOf("\\")+1,webappsPath.length());
            String projectName = projectPath.substring(0,projectPath.indexOf("\\"));

            String separator = File.separator;
            path = System.getProperty("user.dir").replace("bin",tomcatWebapp) + separator + projectName;
            return path;
        }

        path = System.getProperty("user.dir");
        return path;
    }

    /**
     * 获取项目resources路径
     * @return
     */
    public String getResourcesPath(){
        return new File(this.getClass().getResource("/").getPath()).toString();
    }

    /**
     * 创建文本文件
     *
     * @param path
     * @param name
     * @return
     */
    public static boolean creatTxtFile(String path,String name) {
        boolean flag = false;
        try {
            File filename = new File(path+name);
            File file = new File(path);
            file.mkdirs();
            if (!filename.exists()) {
                filename.createNewFile();
                flag = true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return flag;
    }

    /**
     * 修改读取txt文件
     *
     * @param path
     * @param content
     * @param type true = 拼接，false = 覆盖
     * @return
     */
    public static boolean writeTxtFile(String path,String content,boolean type) {
        // 先读取原有文件内容，然后进行写入操作
        boolean flag = false;
        String filein = content + "\r\n";
        String temp = "";

        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;

        FileOutputStream fos = null;
        PrintWriter pw = null;

        try {
            // 文件路径
            File file = new File(path);
            // 将文件读入输入流
            fis = new FileInputStream(file);
            isr = new InputStreamReader(fis);
            br = new BufferedReader(isr);
            StringBuffer buf = new StringBuffer();

            if(type){
                // 保存该文件原有的内容
                for (int j = 1; (temp = br.readLine()) != null; j++) {
                    buf = buf.append(temp);
                    // System.getProperty("line.separator")
                    // 行与行之间的分隔符 相当于“\n”
                    buf = buf.append(System.getProperty("line.separator"));
                }
            }
            //正常替换
            buf.append(filein);

            fos = new FileOutputStream(file);
            pw = new PrintWriter(fos);
            pw.write(buf.toString().toCharArray());
            pw.flush();
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (pw != null) {
                pw.close();
            }
            try {
                if (fos != null) {
                    fos.close();
                }
                if (br != null) {
                    br.close();
                }
                if (isr != null) {
                    isr.close();
                }
                if (fis != null) {
                    fis.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return flag;
    }


    /**
     * 发送QQ邮箱
     *
     * @param email
     * @param emailTitle
     * @param emailValue
     */
    public static void sendEmail(String email,String emailTitle,String emailValue) {
        try {
            Properties properties = new Properties();
            properties.put("mail.transport.protocol", "smtp");// 连接协议
            properties.put("mail.smtp.host", "smtp.qq.com");// 主机名
            properties.put("mail.smtp.port", 465);// 端口号
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.ssl.enable", "true");// 设置是否使用ssl安全连接 ---一般都使用
            properties.put("mail.debug", "true");// 设置是否显示debug信息 true 会在控制台显示相关信息
            Session session = Session.getInstance(properties);
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("aixtport@qq.com"));
            message.setRecipients(Message.RecipientType.TO, new InternetAddress[]{new InternetAddress(email)});
            message.setSubject(emailTitle);
            message.setText(emailValue);

            Transport transport = session.getTransport();
            transport.connect("aixtport@qq.com", "oluecuvgzigmdfbh");// 密码为QQ邮箱开通的stmp服务后得到的客户端授权码
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送GetHttpClient请求
     *
     * @return
     */
    public static String sendGetHttpClient(String url){
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        //System.out.println("Get："+httpGet.getURI());
        CloseableHttpResponse httpResponse = null;
        String content = "";
        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity entity =httpResponse.getEntity();
            if(entity!=null) {
                //System.out.println("响应状态："+httpResponse.getStatusLine());
                content = EntityUtils.toString(entity,"utf-8");
                //System.err.println("响应内容："+content);
                //System.out.println("内容长度："+content.length());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if(httpResponse!=null) {
                try {
                    httpResponse.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return content;
    }

    /**
     * 发送发送PostHttpClient请求
     *
     * @param url
     * @param json
     * @return
     */
    public static String sendPostHttpClient(String url,String json){
        String result = "";
        HttpPost httpPost = new HttpPost(url);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        try {
            BasicResponseHandler handler = new BasicResponseHandler();
            StringEntity entity = new StringEntity(json, "utf-8");//解决中文乱码问题
            entity.setContentEncoding("UTF-8");
            entity.setContentType("application/json");
            httpPost.setEntity(entity);
            result = httpClient.execute(httpPost, handler);
            return result;
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                httpClient.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 发送xml（报文）请求
     * @param url
     */
    public void sendXml(String url){
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
        System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
        System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "stdout");

        StringBuilder sb = new StringBuilder();
        sb.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:fres='http://www.freshpower.com.cn'>");
        sb.append("   <soapenv:Header/>");
        sb.append("   <soapenv:Body>");
        sb.append("      <fres:sendSMS>");
        sb.append("         <fres:in0>{'orgCode':'MHAPP110000','sysCode':'D7cnHmNYXVQEaR1b','data':{ 'sender':'门户APP','receiverphone':'18888888888','messagecontent':'安全监管临期提醒，您还有未巡检任务哦，请您扫码确认，逾期后不可恢复。'}}</fres:in0>");
        sb.append("      </fres:sendSMS>");
        sb.append("   </soapenv:Body>");
        sb.append("</soapenv:Envelope>");

        String xmlString = sb.toString();

        HttpClient client = new HttpClient();
        PostMethod myPost = new PostMethod(url);
        client.getParams().setSoTimeout(300*1000);
        String responseString = null;
        try{
            myPost.setRequestEntity(new StringRequestEntity(xmlString,"text/xml","utf-8"));
            int statusCode = client.executeMethod(myPost);
            if(statusCode == HttpStatus.SC_OK){
                BufferedInputStream bis = new BufferedInputStream(myPost.getResponseBodyAsStream());
                byte[] bytes = new byte[1024];
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                int count = 0;
                while((count = bis.read(bytes))!= -1){
                    bos.write(bytes, 0, count);
                }
                byte[] strByte = bos.toByteArray();
                responseString = new String(strByte,0,strByte.length,"utf-8");
                bos.close();
                bis.close();
            }
        }catch (Exception e) {
            //log.error(e.getMessage(), e);
        }
        myPost.releaseConnection();
        client.getHttpConnectionManager().closeIdleConnections(0);
        System.out.println("responseString:"+responseString);
    }



    /**
     * 发送QQ邮箱 可以发送html的邮件
     * 详情参考：https://www.nhooo.com/note/qa5fap.html
     * 
     * 相关邮件模板，网上找吧，一找一大堆！都挺好看的
     *
     * @param email
     * @param emailTitle
     * @param emailValue
     */
    public static void sendEmailHtml(String email,String emailTitle,String emailValue) {
        try {
            Properties properties = new Properties();
            properties.put("mail.transport.protocol", "smtp");// 连接协议
            properties.put("mail.smtp.host", "smtp.qq.com");// 主机名
            properties.put("mail.smtp.port", 465);// 端口号
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.ssl.enable", "true");// 设置是否使用ssl安全连接 ---一般都使用
            properties.put("mail.debug", "true");// 设置是否显示debug信息 true 会在控制台显示相关信息
            Session session = Session.getInstance(properties);
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("super_tongyao@foxmail.com"));
            message.setRecipients(Message.RecipientType.TO, new InternetAddress[]{new InternetAddress(email)});
            message.setSubject(emailTitle);

            message.setContent(emailValue, "text/html; charset=utf-8");

            Transport transport = session.getTransport();
            transport.connect("super_tongyao@foxmail.com", "upnqzcrqejnecice");// 密码为QQ邮箱开通的stmp服务后得到的客户端授权码
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getWarnHtmlMail(){
        StringBuffer sb = new StringBuffer();
        sb.append("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "\n" +
                "<head>\n" +
                "  <meta charset=\"UTF-8\">\n" +
                "  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "  <title>重置您的 无敌软件的 密码</title>\n" +
                "\n" +
                "  <style>\n" +
                "    body,html,div,ul,li,button,p,img,h1,h2,h3,h4,h5,h6 {\n" +
                "      margin: 0;\n" +
                "      padding: 0;\n" +
                "    }\n" +
                "\n" +
                "    body,html {\n" +
                "      background: #fff;\n" +
                "      line-height: 1.8;\n" +
                "    }\n" +
                "\n" +
                "    h1,h2,h3,h4,h5,h6 {\n" +
                "      line-height: 1.8;\n" +
                "    }\n" +
                "\n" +
                "    .email_warp {\n" +
                "      height: 100vh;\n" +
                "      min-height: 500px;\n" +
                "      font-size: 14px;\n" +
                "      color: #212121;\n" +
                "      display: flex;\n" +
                "      /* align-items: center; */\n" +
                "      justify-content: center;\n" +
                "    }\n" +
                "\n" +
                "    .logo {\n" +
                "      margin: 3em auto;\n" +
                "      width: 200px;\n" +
                "      height: 60px;\n" +
                "    }\n" +
                "\n" +
                "    h1.email-title {\n" +
                "      font-size: 26px;\n" +
                "      font-weight: 500;\n" +
                "      margin-bottom: 15px;\n" +
                "      color: #252525;\n" +
                "    }\n" +
                "\n" +
                "    a.links_btn {\n" +
                "      border: 0;\n" +
                "      background: #4C84FF;\n" +
                "      color: #fff;\n" +
                "      width: 100%;\n" +
                "      height: 50px;\n" +
                "      line-height: 50px;\n" +
                "      font-size: 16px;\n" +
                "      margin: 40px auto;\n" +
                "      box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.15);\n" +
                "      border-radius: 4px;\n" +
                "      outline: none;\n" +
                "      cursor: pointer;\n" +
                "      transition: all 0.3s;\n" +
                "      text-align: center;\n" +
                "      display: block;\n" +
                "      text-decoration: none;\n" +
                "    }\n" +
                "\n" +
                "    .warm_tips {\n" +
                "      color: #757575;\n" +
                "      background: #f7f7f7;\n" +
                "      padding: 20px;\n" +
                "    }\n" +
                "\n" +
                "    .warm_tips .desc {\n" +
                "      margin-bottom: 20px;\n" +
                "    }\n" +
                "\n" +
                "    .qr_warp {\n" +
                "      max-width: 140px;\n" +
                "      margin: 20px auto;\n" +
                "    }\n" +
                "\n" +
                "    .qr_warp img {\n" +
                "      max-width: 100%;\n" +
                "      max-height: 100%;\n" +
                "    }\n" +
                "\n" +
                "    .email-footer {\n" +
                "      margin-top: 2em;\n" +
                "    }\n" +
                "\n" +
                "    #reset-password-email {\n" +
                "      max-width: 500px;\n" +
                "    }\n" +
                "    #reset-password-email .accout_email {\n" +
                "      color: #4C84FF;\n" +
                "      display: block;\n" +
                "      margin-bottom: 20px;\n" +
                "    }\n" +
                "  </style>\n" +
                "</head>\n" +
                "\n" +
                "<body>\n" +
                "  <section class=\"email_warp\">\n" +
                "    <div id=\"reset-password-email\">\n" +
                "      <div class=\"logo\">\n" +
                "        <img src=\"https://lf3-cdn-tos.bytescm.com/obj/static/xitu_juejin_web/dcec27cc6ece0eb5bb217e62e6bec104.svg\" alt=\"logo\">\n" +
                "      </div>\n" +
                "\n" +
                "      <h1 class=\"email-title\">\n" +
                "        尊敬的<span>AAA</span>您好：\n" +
                "      </h1>\n" +
                "      <p>您正在为登录邮箱为如下地址的 啦啦啦 账户重置密码：</p>\n" +
                "      <b class=\"accout_email\">xxxx@abc.com</b>\n" +
                "\n" +
                "      <p>请注意，如果这不是您本人的操作，请忽略并关闭此邮件。</p>\n" +
                "      <p>如您确认重置 XXXX 的账户密码，请点击下方按钮。</p>\n" +
                "\n" +
                "      <a class=\"links_btn\" onclick=\"window.open('https:XXXXXXXXXXX')\">重置密码</a>\n" +
                "\n" +
                "      <div class=\"warm_tips\">\n" +
                "        <div class=\"desc\">\n" +
                "          为安全起见，以上按钮为一次性链接，且仅在24小时内有效，请您尽快完成操作。\n" +
                "        </div>\n" +
                "\n" +
                "        <p>如有任何疑问或无法完成注册，请通过如下方式与我们联系：</p>\n" +
                "        <p>邮箱：support@XXX.cn</p>\n" +
                "        <p>微信助理：XXXXX</p>\n" +
                "\n" +
                "        <div class=\"qr_warp\">\n" +
                "          <img src=\"https://XXXXXXXXXXXX\" alt=\"微信二维码图片\">\n" +
                "        </div>\n" +
                "        <p>本邮件由系统自动发送，请勿回复。</p>\n" +
                "      </div>\n" +
                "\n" +
                "      <div class=\"email-footer\">\n" +
                "        <p>您的智能项目助理</p>\n" +
                "        <p>XXXXXXXX</p>\n" +
                "      </div>\n" +
                "    </div>\n" +
                "  </section>\n" +
                "</body>\n" +
                "\n" +
                "</html>\n" +
                "\n");
        return sb.toString();
    }


    /**
     * 网易免费企业邮箱客户端（pop、imap、smtp）权限默认开启，对应服务器地址为：
     *
     * 发件服务器：
     * SMTP：smtp.ym.163.com 默认端口为：25 （如勾选ssl安全链接，端口号为994）
     * 收件服务器：
     * POP3：pop.ym.163.com 默认端口为：110 （如勾选ssl安全链接，端口号为995）
     * IMAP：imap.ym.163.com 默认端口为：143 （如勾选ssl安全链接，端口号为993）
     *
     * 网易免费企业邮箱没有客户端授权码功能，请直接输入邮箱帐号的密码登录即可。
     *
     *
     * 网易免费企业邮箱帮助地址：https://qiye.163.com/help/l-1.html
     * @param email
     * @param emailTitle
     * @param emailValue
     */
    public static void sendEmailHtml2(String email,String emailTitle,String emailValue) {
        String fromEmail = "xxxxxxx@xxx.cn";
        String fromEmailName = "字节小柜";
        String fromEmailPass = "***";
        try {
            Properties properties = new Properties();
            properties.put("mail.transport.protocol", "smtp");// 连接协议
            properties.put("mail.smtp.host", "smtp.ym.163.com");// 主机名
            properties.put("mail.smtp.port", 994);// 端口号
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.ssl.enable", "true");// 设置是否使用ssl安全连接 ---一般都使用
            properties.put("mail.debug", "true");// 设置是否显示debug信息 true 会在控制台显示相关信息
            Session session = Session.getInstance(properties);
            Message message = new MimeMessage(session);
            //message.setFrom(new InternetAddress("service@ityao.cn"));

            //设置自定义发件人昵称
            message.setFrom(new InternetAddress(javax.mail.internet.MimeUtility.encodeText(fromEmailName)+" <"+fromEmail+">"));

            message.setRecipients(Message.RecipientType.TO, new InternetAddress[]{new InternetAddress(email)});
            message.setSubject(emailTitle);

            message.setContent(emailValue, "text/html; charset=utf-8");

            Transport transport = session.getTransport();
            transport.connect(fromEmail, fromEmailPass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 需提前引入 dom4j相关依赖
     * 
     * xml文件：
     * <?xml version="1.0" encoding="UTF-8" ?>
     * <books>
     *     <book sn="001">
     *         <name>海的女儿</name>
     *         <author>安徒生</author>
     *         <price>0.43</price>
     *     </book>
     *     <book sn="002">
     *         <name>故事新编</name>
     *         <author>鲁迅</author>
     *         <price>0.31</price>
     *     </book>
     * </books>
     * 
     * @throws Exception
     */
    public void readXml() throws Exception{
        // 创建SAXReader实例
        SAXReader reader = new SAXReader();
        // read()读取指定的XML文档并形成DOM树
        Document document = reader.read(new File("D:\\books.xml"));

        // Document document = new SAXReader().read(new ByteArrayInputStream("你的字符xml内容".getBytes("UTF-8")));

        // getRootElement()获取根节点
        Element rootEle = document.getRootElement();
        // elements()获取根节点的子节点
        List<Element> bookEles = rootEle.elements();
        // 遍历子节点
        for (Element book : bookEles
        ) {
            // element()获取子节点指定的子元素
            Element nameElement = book.element("name");
            // getText()获取子元素的文本内容
            String nameText = nameElement.getText();
            // elementText()直接获取元素的文本内容
            String authorText = book.elementText("author");
            String priceText = book.elementText("price");
            // attributeValue()直接获取元素的属性值
            String snValue = book.attributeValue("sn");
            System.out.println(authorText);
        }
    }


}
