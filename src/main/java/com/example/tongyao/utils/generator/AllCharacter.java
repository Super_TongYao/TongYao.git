package com.example.tongyao.utils.generator;

import java.util.Random;
/**
 * 字母和数字的字符数组（全字符）
 *
 * @version 1.0
 * @author tongyao
 * @since 2020-06-13
 */
public class AllCharacter {

    /**
     * 定义全字符（包含数字，大小写字母）
     * @return 返回字符数组
     */
    public static char[] charArray(){
        int arabicNumerals = 1234567890;
        String letterLowerCase ="qwertyuiopasdfghjklzxcvbnm";
        String letterUpperCase=letterLowerCase.toUpperCase();
        String fullCharacter=letterLowerCase+letterUpperCase+arabicNumerals;
        char[] charArray=fullCharacter.toCharArray();
        return charArray;
    }


    /**
     * 生成随机码（随机码包括数字,大小写字母
     * @param length 随机码长度
     * @return 返回生成的随机码
     */
    public static String randomCode(int length){

        char[] charArray= charArray();//获取包含26个字母大小写和数字的字符数组

        Random rd = new Random();
        String code="";
        for (int k = 1; k <= length; k++) {
            int index = rd.nextInt(charArray.length);//随机获取数组长度作为索引
            code+=charArray[index];//循环添加到字符串后面
        }
        return code;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            String s = randomCode(6);
            System.out.println(s);
        }
    }
}