package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色菜单关系表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2020-06-13
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
