package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2020-06-13
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    //根据用户名查找数据，用作登录。
    SysUser findByUsername(@Param("username") String username);

    //查询所有用户
    List<SysUser> getList();
}
