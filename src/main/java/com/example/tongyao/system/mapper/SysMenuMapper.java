package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2020-06-13
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<SysMenu> byUserNameQueryMenu(String userName);
}
