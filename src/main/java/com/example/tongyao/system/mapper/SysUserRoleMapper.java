package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色关系表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2020-06-13
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
