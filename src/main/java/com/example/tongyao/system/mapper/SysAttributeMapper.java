package com.example.tongyao.system.mapper;

import com.example.tongyao.system.entity.SysAttribute;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 属性表 Mapper 接口
 * </p>
 *
 * @author tongyao
 * @since 2021-06-19
 */
public interface SysAttributeMapper extends BaseMapper<SysAttribute> {

    /*根据键获得值*/
    SysAttribute byKeyGetValue(String key);
}
