package com.example.tongyao.system.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.tongyao.system.entity.SysRole;
import com.example.tongyao.system.entity.SysUser;
import com.example.tongyao.system.service.ISysRoleMenuService;
import com.example.tongyao.system.service.ISysRoleService;
import com.example.tongyao.utils.DataResult;
import com.example.tongyao.utils.tools.PubClass;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author tongyao
 * @since 2020-06-13
 */
@RestController
@Api(tags = "系统 - 角色管理")
@RequestMapping("/system/sys-role")
public class SysRoleController {

    @Autowired
    private ISysRoleService iSysRoleService;

    @Autowired
    private ISysRoleMenuService iSysRoleMenuService;

    @ApiOperation(
            value = "1、增加角色",
            notes = "增加角色",
            httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sysRole",value = "要添加的用户信息",required = true),
    })
    @RequiresPermissions("role")
    @PostMapping(value = "/add" , produces = {MediaType.APPLICATION_JSON_VALUE})
    public DataResult add(@RequestBody(required = true) SysRole sysRole) {
        //获取登录的用户信息
        SysUser sessionUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        sysRole.setCreateBy(sessionUser.getUserName());
        sysRole.setCreateTime(PubClass.getDateTime());
        boolean isResult = iSysRoleService.save(sysRole);
        if(!isResult){
            return DataResult.setError("角色添加失败！");
        }
        return DataResult.setSuccess("添加成功！");
    }

    @ApiOperation(
            value = "2、批量删除角色信息",
            notes = "批量删除角色信息",
            httpMethod = "DELETE")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids",value = "删除的id数组，以小写逗号隔开",required = true),
    })
    @RequiresPermissions("role")
    @DeleteMapping(value = "/delete" , produces = {MediaType.APPLICATION_JSON_VALUE})// 请求
    public DataResult delete(@RequestParam(required = true) String ids) {
        if(StringUtils.isEmpty(ids)){
            return DataResult.setError("用户ids为空！");
        }
        String[] id = ids.split(",");
        List<String> idS = new ArrayList<>(Arrays.asList(id));
        iSysRoleService.removeByIds(idS);

        Map<String,Object> map = new HashMap<>();
        for(String str : idS){
            map.put("role_id",str);
            iSysRoleMenuService.removeByMap(map);
        }
        return DataResult.setSuccess("删除成功！");
    }

    @ApiOperation(
            value = "3、修改角色信息",
            notes = "修改角色信息",
            httpMethod = "PUT")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sysRole",value = "要修改的用户总体信息",required = true),
    })
    @RequiresPermissions("role")
    @PutMapping(value = "/update")// 请求
    public DataResult update(@RequestBody(required = true) SysRole sysRole) {
        //获取登录的用户信息
        SysUser sessionUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        sysRole.setModifyBy(sessionUser.getUserName());
        sysRole.setModifyTime(PubClass.getDateTime());
        boolean isResult = iSysRoleService.updateById(sysRole);
        if (!isResult){
            return DataResult.setError("用户信息更改失败！");
        }
        return DataResult.setSuccess("修改成功！");
    }

    @ApiOperation(
            value = "4、更新角色状态",
            notes = "通过传过来的角色id，修改账号的状态",
            httpMethod = "PUT")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "roleId",value = "用户id",required = true),
            @ApiImplicitParam(name = "status",value = "状态true或false",required = true)
    })
    @RequiresPermissions("role")
    @PutMapping(value = "/updateStatus")// 请求
    public DataResult updateStatus(@RequestParam(required = true) String roleId,
                                   @RequestParam(required = true,defaultValue = "true") boolean status ) {
        if(StringUtils.isEmpty(roleId)){
            return DataResult.setError("角色Id为空！");
        }
        LambdaUpdateWrapper<SysRole> sysUserLambdaUpdateWrapper =new LambdaUpdateWrapper<>();
        sysUserLambdaUpdateWrapper.set(SysRole::getRoleStatus,status).eq(SysRole::getId,roleId);
        boolean isResult = iSysRoleService.update(sysUserLambdaUpdateWrapper);
        if (!isResult){
            return DataResult.setError("状态更改失败！");
        }
        return DataResult.setSuccess("状态更改成功！");
    }

    /**
     * 分页查询所有角色
     * @return
     */
    @ApiOperation(
            value = "5、分页查询所有角色",
            notes = "查询所有角色",
            httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo",value = "当前页数",defaultValue = "1",required = false),
            @ApiImplicitParam(name = "pageSize",value = "显示条数",defaultValue = "10",required = false),
    })
    @RequiresPermissions("role")
    @GetMapping(value = "/page" , produces = {MediaType.APPLICATION_JSON_VALUE})
    public DataResult page(@RequestParam(required = false,defaultValue = "1") int pageNo,
                           @RequestParam(required = false,defaultValue = "10") int pageSize) {
        LambdaQueryWrapper<SysRole> sysUserLambdaQueryWrapper = new LambdaQueryWrapper<>();

        Page<SysRole> page = new Page<>(pageNo,pageSize);
        IPage<SysRole> iPage = this.iSysRoleService.page(page,sysUserLambdaQueryWrapper);
        return DataResult.setSuccess(iPage);
    }
}
