package com.example.tongyao.system.controller;


import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.example.tongyao.system.entity.SysMenu;
import com.example.tongyao.system.entity.SysRoleMenu;
import com.example.tongyao.system.service.ISysMenuService;
import com.example.tongyao.system.service.ISysRoleMenuService;
import com.example.tongyao.utils.DataResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * <p>
 * 角色菜单关系表 前端控制器
 * </p>
 *
 * @author tongyao
 * @since 2020-06-13
 */
@RestController
@Api(tags = "系统 - 角色菜单管理")
@RequestMapping("/system/sys-role-menu")
public class SysRoleMenuController {

    @Autowired
    private ISysRoleMenuService iSysRoleMenuService;

    @Autowired
    private ISysMenuService iSysMenuService;

    /**
     * 查询该角色所拥有的菜单权限
     * @return
     */
    @ApiOperation(
            value = "1、查询该角色所拥有的菜单权限",
            notes = "查询该角色所拥有的菜单权限",
            httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "roleId",value = "角色id",required = true),
    })
    @RequiresPermissions("role")
    @GetMapping(value = "/listRoleMenu" , produces = {MediaType.APPLICATION_JSON_VALUE})
    public DataResult listRoleMenu(@RequestParam(required = true) String roleId) {
        if(StringUtils.isEmpty(roleId)){
            return DataResult.setError("角色Id为空！");
        }
        //查出该权限所拥有的菜单ID
        Map<String,Object> map = new HashMap<>();
        map.put("role_id",roleId);
        List<SysRoleMenu> sysRoleMenuList = (List<SysRoleMenu>) iSysRoleMenuService.listByMap(map);

        //获取所有菜单ID
        List<SysMenu> sysMenuList = iSysMenuService.list();

        for(int i = 0;i< sysMenuList.size();i++){
            for(int k = 0;k < sysRoleMenuList.size();k++){
                if(sysRoleMenuList.get(k).getMenuId().equals(sysMenuList.get(i).getId())){
                    sysMenuList.get(i).setChecked(true);
                    break;
                }
            }
        }

        sysMenuList = sortMenu(sysMenuList,"0");
        return DataResult.setSuccess(sysMenuList);
    }

    /**
     *  调整菜单
     * @param list
     * @param parentId
     * @return
     */
    private List<SysMenu> sortMenu(List<SysMenu> list,String parentId){//递归塞值
        List<SysMenu> returnList = new ArrayList<>();
        for (SysMenu menu:list) {
            if(parentId.equals(menu.getParentId())){
                menu.setChildrenItem(sortMenu(list,menu.getId()));
                returnList.add(menu);
            }
        }
        return returnList;
    }

    /**
     * 查询该角色所拥有的菜单权限
     * @return
     */
    @ApiOperation(
            value = "2、给角色添加相应菜单权限",
            notes = "给角色添加相应菜单权限",
            httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "roleId",value = "角色id",required = true),
            @ApiImplicitParam(name = "menuIds",value = "菜单id集合",required = true),
    })
    @RequiresPermissions("role")
    @PostMapping(value = "/add" , produces = {MediaType.APPLICATION_JSON_VALUE})
    public DataResult add(@RequestParam(required = true) String roleId,
                          @RequestParam(required = true) String menuIds) {
        if(StringUtils.isEmpty(roleId)){
            return DataResult.setError("角色Id为空！");
        }
        if(StringUtils.isEmpty(menuIds)){
            return DataResult.setError("菜单Id集合为空！");
        }

        SysRoleMenu sysRoleMenu;
        String[] id = menuIds.split(",");
        List<String> ids = new ArrayList<>(Arrays.asList(id));
        for (String str : ids){
            sysRoleMenu = new SysRoleMenu();
            sysRoleMenu.setMenuId(str);
            sysRoleMenu.setRoleId(roleId);
            iSysRoleMenuService.save(sysRoleMenu);
        }
        return DataResult.setSuccess("增加成功！");
    }

}
