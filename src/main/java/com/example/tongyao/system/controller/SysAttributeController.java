package com.example.tongyao.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 属性表 前端控制器
 * </p>
 *
 * @author tongyao
 * @since 2021-06-19
 */
@RestController
@RequestMapping("/system/sys-attribute")
public class SysAttributeController {

}
