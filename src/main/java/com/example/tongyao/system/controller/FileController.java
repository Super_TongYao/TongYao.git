package com.example.tongyao.system.controller;

import com.example.tongyao.utils.DataResult;
import io.swagger.annotations.Api;
import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;

/**
 * tongyao >>> 【com.example.tongyao.system.controller】
 * 文件上传处理类
 *
 * @author: tongyao
 * @since: 2021-4-23 20:13
 */
@RestController
@Api(tags = "系统 - 文件上传处理管理")
@RequestMapping("/system/sys-file")
public class FileController {

    /**
     * 文件上传
     * @param pictures
     * @return
     * @throws Exception
     */
    public DataResult fileUpload(MultipartFile[] pictures) throws Exception{
        //数据库存储的路径
        String dataPath = "";

        File targetFile = null;
        FileOutputStream fileOutputStream = null;
        String fileUuid = "";
        String fileName = "";
        String fileType = "";
        for(int i = 0 ;i<pictures.length;i++){
            //随机生成文件名称
            fileUuid = UUID.randomUUID().toString();
            //获取到文件总名称
            fileName = pictures[i].getOriginalFilename();
            if(fileName.equals("")){
                break;
            }
            //获取文件名称后缀
            fileType = fileName.substring(fileName.lastIndexOf("."),fileName.length());
            //文件后缀可以考虑配置放到resource资源yml文件，循环读取
            if(!fileType.equals(".jpg") && !fileType.equals(".png") ){
                return DataResult.setResult("不是图片格式，图片仅支持png、jpg！");
            }
            //获取当前项目包的resources路径
            //存放照片路径可以去yml文件里配置，或者配置的某个字段为true或false来判断是否用配置的绝对路径还是系统默认相对路径，包括文件命名规则都可以配置
            String targetFilePath = new File(this.getClass().getResource("/").getPath()).toString()+"\\static\\images\\file\\pictures\\";
            targetFile = new File(targetFilePath + File.separator + fileUuid+fileType);
            fileOutputStream = new FileOutputStream(targetFile);
            IOUtils.copy(pictures[i].getInputStream(), fileOutputStream);
            IOUtils.closeQuietly(fileOutputStream);
            fileOutputStream.close();
            //拼接数据库存储的路径
            dataPath = dataPath +"/images/file/pictures/"+fileUuid+fileType+",";
        }

        /**
         * 前台使用form保单提交，可以参考一下代码
         * <form class='layui-form' action='./fileUpload' name='form' method='post' enctype="multipart/form-data" target="nm_iframe">
         * 	图片：
         * 	<input type='file' name='pictures' multiple>
         * 	 <button type='button' id='ok' onclick='ok_submit()'>提交</button>
         * </form>
         * <iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>
         *
         * <script>
         * 	function ok_submit(){
         *         //$("#ok").attr("disabled","disabled")
         *         //$("#ok").html("正在提交...")
         *         $("[name=form]").submit()
         *     }
         * 	$("#id_iframe").on("load",function(){
         *         var text = $(this).contents().find("body").text(); //获取到的是json的字符串
         *         var data = $.parseJSON(text);  //json字符串转换成json对象
         *         
         *         //$("#ok").html("确定")
         *         //$("#ok").removeAttr("disabled")
         *         //if(data.data != true){
         *         //    layer.msg(data.data)
         *         //    return false;
         *         //}
         *         layer.msg("添加成功！")
         *     })
         * </script>
         */
        return DataResult.setSuccess(true);
    }


}
