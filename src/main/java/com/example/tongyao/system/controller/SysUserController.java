package com.example.tongyao.system.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.tongyao.system.entity.SysUser;
import com.example.tongyao.system.entity.SysUserRole;
import com.example.tongyao.system.service.ISysUserRoleService;
import com.example.tongyao.system.service.ISysUserService;
import com.example.tongyao.utils.DataResult;
import com.example.tongyao.utils.tools.PubClass;
import com.example.tongyao.utils.encryption.MD5Util;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author tongyao
 * @since 2020-06-13
 */
@RestController
@Api(tags = "系统 - 用户信息管理")
@RequestMapping("/system/sys-user")
public class SysUserController {

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private ISysUserRoleService iSysUserRoleService;

    @ApiOperation(
            value = "1、增加用户",
            notes = "查询所有用户",
            httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sysUser",value = "要添加的用户信息",required = true),
            @ApiImplicitParam(name = "roleId",value = "角色id",required = true),
    })
    @RequiresPermissions("user")
    @PostMapping(value = "/add" , produces = {MediaType.APPLICATION_JSON_VALUE})
    public DataResult add(@RequestBody(required = true) SysUser sysUser,
                           @RequestParam(required = true) String roleId) {
        if(StringUtils.isEmpty(roleId)){
            return DataResult.setError("用户角色为空！");
        }
        //获取登录的用户信息
        SysUser sessionUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        sysUser.setCreateBy(sessionUser.getUserName());
        sysUser.setCreateTime(PubClass.getDateTime());
        sysUser.setUserPassword(MD5Util.string2MD5(sysUser.getUserPassword()));
        boolean isResult = iSysUserService.save(sysUser);
        if(!isResult){
            return DataResult.setError("用户添加失败！");
        }

        SysUserRole sysUserRole = new SysUserRole();
        sysUserRole.setUserId(sysUser.getId());
        sysUserRole.setRoleId(roleId);
        isResult = iSysUserRoleService.save(sysUserRole);
        if(!isResult){
            return DataResult.setError("用户角色关系添加失败！");
        }

        return DataResult.setSuccess("添加成功！");
    }

    @ApiOperation(
            value = "2、批量删除用户信息",
            notes = "批量删除用户信息",
            httpMethod = "DELETE")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids",value = "删除的id数组，以小写逗号隔开",required = true),
    })
    @RequiresPermissions("user")
    @DeleteMapping(value = "/delete" , produces = {MediaType.APPLICATION_JSON_VALUE})// 请求
    public DataResult delete(@RequestParam(required = true) String ids) {
        if(StringUtils.isEmpty(ids)){
            return DataResult.setError("用户ids为空！");
        }
        String[] id = ids.split(",");
        List<String> idS = new ArrayList<>(Arrays.asList(id));
        iSysUserService.removeByIds(idS);

        Map<String,Object> map = new HashMap<>();
        for(String str : idS){
            map.put("user_id",str);
            iSysUserRoleService.removeByMap(map);
        }
        return DataResult.setSuccess("删除成功！");
    }

    @ApiOperation(
            value = "3、修改用户信息",
            notes = "修改用户信息",
            httpMethod = "PUT")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sysUser",value = "要修改的用户总体信息",required = true),
            @ApiImplicitParam(name = "roleId",value = "用户id",required = true),
    })
    @RequiresPermissions("user")
    @PutMapping(value = "/update")// 请求
    public DataResult update(@RequestBody(required = true) SysUser sysUser,
                         @RequestParam(required = true) String roleId) {
        if(StringUtils.isEmpty(roleId)){
            return DataResult.setError("角色id为空！");
        }
        //获取登录的用户信息
        SysUser sessionUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        sysUser.setModifyBy(sessionUser.getUserName());
        sysUser.setModifyTime(PubClass.getDateTime());
        boolean isResult = iSysUserService.updateById(sysUser);
        if (!isResult){
            return DataResult.setError("用户信息更改失败！");
        }

        LambdaUpdateWrapper<SysUserRole> sysUserRoleLambdaUpdateWrapper =new LambdaUpdateWrapper<>();
        sysUserRoleLambdaUpdateWrapper.set(SysUserRole::getRoleId,roleId).eq(SysUserRole::getUserId,sysUser.getId());
        isResult = iSysUserRoleService.update(sysUserRoleLambdaUpdateWrapper);
        if (!isResult){
            return DataResult.setError("角色信息更改失败！");
        }

        return DataResult.setSuccess("修改成功！");
    }

    @ApiOperation(
            value = "4、更新账号状态",
            notes = "通过传过来的账号id，修改账号的状态",
            httpMethod = "PUT")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId",value = "用户id",required = true),
            @ApiImplicitParam(name = "status",value = "状态true或false",required = true)
    })
    @RequiresPermissions("user")
    @PutMapping(value = "/updateStatus")// 请求
    public DataResult updateStatus(@RequestParam(required = true) String userId,
                                   @RequestParam(required = true,defaultValue = "true") boolean status ) {
        if(StringUtils.isEmpty(userId)){
            return DataResult.setError("用户Id为空！");
        }
        LambdaUpdateWrapper<SysUser> sysUserLambdaUpdateWrapper =new LambdaUpdateWrapper<>();
        sysUserLambdaUpdateWrapper.set(SysUser::getStatus,status).eq(SysUser::getId,userId);
        boolean isResult = iSysUserService.update(sysUserLambdaUpdateWrapper);
        if (!isResult){
            return DataResult.setError("状态更改失败！");
        }
        return DataResult.setSuccess("状态更改成功！");
    }

    /**
     * 分页查询所有用户
     * @return
     */
    @ApiOperation(
            value = "5、分页查询所有用户",
            notes = "查询所有用户",
            httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo",value = "当前页数",defaultValue = "1",required = false),
            @ApiImplicitParam(name = "pageSize",value = "显示条数",defaultValue = "10",required = false),
    })
    @RequiresPermissions("user")
    @GetMapping(value = "/page" , produces = {MediaType.APPLICATION_JSON_VALUE})
    public DataResult page(@RequestParam(required = false,defaultValue = "1") int pageNo,
                           @RequestParam(required = false,defaultValue = "10") int pageSize) {
        LambdaQueryWrapper<SysUser> sysUserLambdaQueryWrapper = new LambdaQueryWrapper<>();

        Page<SysUser> page = new Page<>(pageNo,pageSize);
        IPage<SysUser> iPage = this.iSysUserService.page(page,sysUserLambdaQueryWrapper);
        return DataResult.setSuccess(iPage);
    }

}
