package com.example.tongyao.system.config;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;

/**
 * shiro登录校验
 *
 * @version 1.0
 * @author tongyao
 * @since 2020-06-13
 */
public class CredentialMatcher extends SimpleCredentialsMatcher {
    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) token;
        //从浏览器中获取的密码
        String password = new String(usernamePasswordToken.getPassword());
        
        //根据username查出密码，最后进行判断
        String dbPassword = (String) info.getCredentials();
        return this.equals(password, dbPassword);
    }
}