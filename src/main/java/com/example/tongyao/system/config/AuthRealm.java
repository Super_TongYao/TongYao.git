package com.example.tongyao.system.config;

import com.example.tongyao.system.entity.SysMenu;
import com.example.tongyao.system.entity.SysUser;
import com.example.tongyao.system.service.ISysMenuService;
import com.example.tongyao.system.service.ISysRoleService;
import com.example.tongyao.system.service.ISysUserRoleService;
import com.example.tongyao.system.service.ISysUserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * AuthRealm
 *
 * @version 1.0
 * @author tongyao
 * @since 2020-06-13
 */
public class AuthRealm extends AuthorizingRealm {

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private ISysRoleService iSysRoleService;

    @Autowired
    private ISysMenuService iSysMenuService;

    /**
     * 授权
     * @param principals
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        SysUser sysUser = (SysUser) principals.fromRealm(this.getClass().getName()).iterator().next();
        
        /*List<String> permissionList = new ArrayList<>();
        List<String> roleNameList = new ArrayList<>();
        Set<SysRole> roleSet = user.getRoles();
        if (CollectionUtils.isNotEmpty(roleSet)) {
            for(Role role : roleSet) {
                roleNameList.add(role.getName());
                Set<Permission> permissionSet = role.getPermissions();
                if (CollectionUtils.isNotEmpty(permissionSet)) {
                    for (Permission permission : permissionSet) {
                        permissionList.add(permission.getName());
                    }
                }
            }
        }*/

        // 赋予角色
        SysUser sysMenu = (SysUser)principals.getPrimaryPrincipal();
        String userId = sysMenu.getUserName();
        List<String> listRole = iSysRoleService.byUserNameQueryRole(userId);
        for (String str : listRole) {
            info.addRole(str);
        }

        List<SysMenu> listMenu = iSysMenuService.byUserNameQueryMenu(userId);
        //根据用户账号查询所有菜单
        if (!org.springframework.util.CollectionUtils.isEmpty(listMenu)) {
            Set<String> permissionSet = new HashSet();
            for (SysMenu menu : listMenu) {
                String permission = null;
                if (!StringUtils.isEmpty(permission = menu.getPermission())) {
                    permissionSet.addAll(Arrays.asList(permission.trim().split(",")));
                }
            }
            info.setStringPermissions(permissionSet);
        }
        return info;
    }

    /**
     * 认证登录
     * @param token
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) token;
        //从浏览器地址栏获取用户名
        String username = usernamePasswordToken.getUsername();
        SysUser user = iSysUserService.findByUsername(username);
        
        return new SimpleAuthenticationInfo(user, user.getUserPassword(), this.getClass().getName());
    }
}