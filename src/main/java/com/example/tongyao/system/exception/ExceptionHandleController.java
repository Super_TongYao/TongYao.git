/**
 * MIT License
 * Copyright (c) 2018 yadong.zhang
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.example.tongyao.system.exception;

import com.example.tongyao.utils.DataResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.http.HttpStatus;
import org.apache.shiro.authz.AuthorizationException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 统一异常处理类
 * 捕获程序所有异常，针对不同异常，采取不同的处理方式
 *
 * @version 1.0
 * @author tongyao
 * @since 2020-06-13
 */
@Api(tags = "系统 - 异常控制层")
@ControllerAdvice
@RestController
public class ExceptionHandleController {

    /**
     * 后台全局捕获异常
     * @param e
     * @return
     */
    @ApiOperation(value = "handle", notes = "无权限或未知异常进入的方法")
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public DataResult handle(Throwable e) {
        if(e instanceof AuthorizationException){
            return DataResult.setResult(HttpStatus.SC_UNAUTHORIZED,"您没有权限！");
        }
        return DataResult.setResult(HttpStatus.SC_INTERNAL_SERVER_ERROR,"未知异常："+e.getMessage());
    }

    /***
     * 404处理
     * @param e
     * @return
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseBody
    @ResponseStatus(org.springframework.http.HttpStatus.NOT_FOUND)
    public Object notFountHandler(HttpServletRequest request, NoHandlerFoundException e){
        String method = request.getMethod();
        String path = request.getRequestURI();
        Map<String,Object> data = new HashMap();
        data.put("code","404");
        data.put("method",method);
        data.put("path",path);
        data.put("desc","404啦");
        return data;
    }

    /**
     * 无登录状态提示的
     * @return
     */
    @ApiOperation(value = "defaultLogin", notes = "无登录状态进入的方法")
    @GetMapping("/defaultLogin")
    public DataResult defaultLogin(){
        return DataResult.setResult(HttpStatus.SC_UNAUTHORIZED,"您没有登录或者您的登录操作已过期！请登录后再次访问。");
    }

}
