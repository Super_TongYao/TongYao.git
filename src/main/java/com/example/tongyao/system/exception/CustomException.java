package com.example.tongyao.system.exception;

import lombok.Data;

/**
 * 自定义异常 继承你想要的异常
 *
 * 抛出异常示例
 * throw new CustomException("test");
 */
@Data
public class CustomException extends Exception {

    private String msg;
    private int code = 500;

    public CustomException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public CustomException(String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
    }

    public CustomException(String msg, int code) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }

    public CustomException(String msg, int code, Throwable e) {
        super(msg, e);
        this.msg = msg;
        this.code = code;
    }

}
