package com.example.tongyao.system.entity;


import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
* <p>
* 用户表
* </p>
*
* @author tongyao
* @since 2020-06-13
*/
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="SysUser对象", description="用户表")
public class SysUser extends SuperEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "编号")
    @TableId(value ="id", type = IdType.UUID)
    private String id;

    @ApiModelProperty(value = "用户账号")
    private String userName;

    @ApiModelProperty(value = "用户密码")
    private String userPassword;

    @ApiModelProperty(value = "用户昵称")
    private String userNickName;

    @ApiModelProperty(value = "头像地址")
    private String avatatAddress;

    @ApiModelProperty(value = "性别")
    private Integer sex;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "出生日期")
    private Date birthdayTime;

    @ApiModelProperty(value = "电话手机")
    private String phone;

    @ApiModelProperty(value = "邮箱地址")
    private String emailAddress;

    @ApiModelProperty(value = "账户状态（1、启用0、禁用）")
    private Boolean status;

    @ApiModelProperty(value = "删除状态（1、没删除0、已删除）")
    private Boolean delFlag;

    @ApiModelProperty(value = "创建者")
    private String createBy;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "修改者")
    private String modifyBy;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "修改时间")
    private Date modifyTime;

    /*@TableField(exist = false)
    @ApiModelProperty(value = "roleId")
    private Integer roleId;

    @TableField(exist = false)
    @ApiModelProperty(value = "roleState")
    private Boolean roleStatus;

    @TableField(exist = false)
    @ApiModelProperty(value = "roleName")
    private String roleName;*/

    @TableField(exist = false)
    @ApiModelProperty(value = "roleState")
    private SysRole sysRole;

    @TableField(exist = false)
    @ApiModelProperty(value = "SessionId")
    private String sessionId;


}
