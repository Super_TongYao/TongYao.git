package com.example.tongyao.system.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.tongyao.common.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
* <p>
* 菜单表
* </p>
*
* @author tongyao
* @since 2020-06-13
*/
@Data
@EqualsAndHashCode
@Accessors(chain = true)
@ApiModel(value="SysMenu对象", description="菜单表")
public class SysMenu extends SuperEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "编号")
    @TableId(value ="id", type = IdType.UUID)
    private String id;

    @ApiModelProperty(value = "父级ID")
    private String parentId;

    @ApiModelProperty(value = "菜单名称")
    private String menuName;

    @ApiModelProperty(value = "权限类型")
    private String menuType;

    @ApiModelProperty(value = "菜单权限")
    private String permission;

    @ApiModelProperty(value = "菜单路径")
    private String menuPath;

    @ApiModelProperty(value = "菜单图标")
    private String menuIcon;

    @ApiModelProperty(value = "打开方式")
    private String openType;

    @ApiModelProperty(value = "菜单状态")
    private Boolean menuStatus;

    @ApiModelProperty(value = "菜单排序")
    private Integer menuSort;

    @ApiModelProperty(value = "面包屑")
    private String crumbs;

    @ApiModelProperty(value = "删除状态")
    private Boolean delFlag;

    @ApiModelProperty(value = "创建者")
    private String createBy;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "修改者")
    private String modifyBy;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "修改时间")
    private Date modifyTime;

    @TableField(exist = false)
    @ApiModelProperty(value = "是否选中")
    private Boolean checked = false;

    @TableField(exist = false)
    @ApiModelProperty(value = "子节点")
    private List<SysMenu> childrenItem;


}
