package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2020-06-13
 */
public interface ISysMenuService extends IService<SysMenu> {

    List<SysMenu> byUserNameQueryMenu(String userName);
}
