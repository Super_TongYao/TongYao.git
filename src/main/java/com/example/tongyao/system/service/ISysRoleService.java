package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2020-06-13
 */
public interface ISysRoleService extends IService<SysRole> {

    List<String> byUserNameQueryRole(String userName);
}
