package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysAttribute;
import com.example.tongyao.system.mapper.SysAttributeMapper;
import com.example.tongyao.system.service.ISysAttributeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 属性表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2021-06-19
 */
@Service
public class SysAttributeServiceImpl extends ServiceImpl<SysAttributeMapper, SysAttribute> implements ISysAttributeService {

    @Resource
    private SysAttributeMapper sysAttributeMapper;

    @Override
    public SysAttribute byKeyGetValue(String key) {
        return sysAttributeMapper.byKeyGetValue(key);
    }
}
