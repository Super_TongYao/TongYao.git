package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色关系表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2020-06-13
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
