package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysAttribute;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 属性表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2021-06-19
 */
public interface ISysAttributeService extends IService<SysAttribute> {

    /*根据键获得值*/
    SysAttribute byKeyGetValue(String key);
}
