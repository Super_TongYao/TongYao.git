package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色菜单关系表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2020-06-13
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

}
