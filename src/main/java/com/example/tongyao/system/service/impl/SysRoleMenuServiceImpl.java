package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysRoleMenu;
import com.example.tongyao.system.mapper.SysRoleMenuMapper;
import com.example.tongyao.system.service.ISysRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色菜单关系表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2020-06-13
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {

}
