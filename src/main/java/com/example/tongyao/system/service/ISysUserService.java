package com.example.tongyao.system.service;

import com.example.tongyao.system.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author tongyao
 * @since 2020-06-13
 */
public interface ISysUserService extends IService<SysUser> {

    List<SysUser> getList();

    SysUser findByUsername(String username);
}
