package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysMenu;
import com.example.tongyao.system.mapper.SysMenuMapper;
import com.example.tongyao.system.service.ISysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2020-06-13
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {

    @Resource
    private SysMenuMapper sysMenuMapper;

    @Override
    public List<SysMenu> byUserNameQueryMenu(String userName) {
        return sysMenuMapper.byUserNameQueryMenu(userName);
    }
}
