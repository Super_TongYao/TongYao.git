package com.example.tongyao.system.service.impl;

import com.example.tongyao.system.entity.SysUserRole;
import com.example.tongyao.system.mapper.SysUserRoleMapper;
import com.example.tongyao.system.service.ISysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色关系表 服务实现类
 * </p>
 *
 * @author tongyao
 * @since 2020-06-13
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
