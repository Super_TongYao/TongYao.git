package com.example.tongyao;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * TongYao框架启动类
 *
 * @author tongyao
 * 声明：尊重开源，开源地址：https://gitee.com/Super_TongYao/TongYao.git
 */
@SpringBootApplication
@EnableSwagger2 //开启Swagger接口文档
@MapperScan(basePackages = {"com.example.tongyao.system.mapper","com.example.tongyao.core.mapper"})
@EnableTransactionManagement //事务控制器
public class TongyaoApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(TongyaoApplication.class, args);
    }

    //打war包启动口
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(TongyaoApplication.class);
    }

}
